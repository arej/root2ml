import os
import yaml
from typing import List
import uproot as ur
import awkward as ak
from termcolor import cprint
import pandas as pd
import numpy as np
from scipy import stats
from datetime import datetime
import dask.dataframe as dd

class_dict = {'signals': 1, 'backgrounds': 0}

def load_yaml(file_name):
    assert(os.path.exists(file_name))
    with open(file_name) as f:
        return yaml.load(f, Loader=yaml.FullLoader)

def find_files(filepath: str,
               filt: str,
               verbose: bool = False) -> List[str]:
    '''
    Helper function to walk through files
    Inputs:
        filepath -> str: Input filepath
        filt     -> str: Filter to check
        verbose  -> bool: Flag for verbose
    Return:
        all_files -> List[str]: List of all files
    '''
    all_files = []
    # Walk through all root files and find and save the filtered ones in a list
    if verbose:
        print('Filtering for', filt, 'in', filepath)
    for root, dirs, files in os.walk(filepath):
        all_files = [root+filenames for filenames in files if filt in filenames]
        for f in all_files:
            if not f.endswith('.root'): continue
    return all_files

def simplify_dict(complex_dict):
    # assuming the dictionary has the following structure:
    # complex_dict = {
    #   "name1": ["value1_string", value1_float, ...], 
    #   "name2": [...], 
    #   ...
    # }
    # return a list of all the dictionaries:
    # simple_dict1 = {
    #   "name1": "value1_string", 
    #   "name2": "value2_string", 
    #   ...
    # }
    # simple_dict2 = {
    #   "name1": value1_float, 
    #   "name2": value2_float, 
    #   ...
    # }
    # ...
    dict_keys_ = list(complex_dict.keys())
    dict_values_ = list(complex_dict.values())
    rank = len(dict_values_[0])
    simple_dict_list = []
    for r in range(rank):
        dict_values_r = [item[r] for item in dict_values_]
        simple_dict_r = dict(zip(dict_keys_, dict_values_r))
        simple_dict_list.append(simple_dict_r)
    return simple_dict_list

def convertROOTtoHDF5(
    filename, 
    start, 
    infolder, 
    outfolder, 
    metadata=[], 
    weight_branches=[], 
    rename_branches_dict={}, # keep these branches with a simplified name, NOT dropped anyway
    tree='reco', 
    all_hists = {}, 
    save_datasets = False,
    prune_outliers = False,
    prune_outliers_dict = {},
    partition_size = 256, #MB
    verbose=False, **kwargs):

    failed = []
    if verbose:
        print('\tUsing filename: ',filename,'\n\tstart: ',start,'\n\tinfolder: ',infolder,'\n\toutfolder: ',outfolder,'\n\tTTree: ',tree,'\n')

    time_00 = datetime.now()

    i = start
    for data in ur.iterate(filename+':'+tree, **kwargs):
        if verbose:
            print(f'\tchecking {filename} -> {tree} TTree ...')

        # Loop over files, dump to h5 file
        try:
            # Dump to dataframe and flatten
            # data = ak.to_pandas(data) 
            data = ak.to_dataframe(data) # with awkward 2 `to_dataframe` can be used instead as it's 'better'
            data = data.droplevel(1)


            # prune outlier entries for selected features
            if prune_outliers_dict and prune_outliers:
                feature_branches_to_prune = [k for k, v in prune_outliers_dict.items() if v==True]
                for feature in feature_branches_to_prune:
                    data = data[np.abs(stats.zscore(data[feature].values))<5]


            # adding event weight variable multiplying weight branches with the metadata factor
            # and removing the individual weight branches 
            # and changing the branch names as specified in configuration
            data['eventWeight'] = metadata[0] #metadata_factor_dsid_camp
            data['dsid'] = metadata[1]

            for l in range(len(weight_branches)):
                data['eventWeight'] *= data[weight_branches[l]]
            if rename_branches_dict:
                data.rename(columns=rename_branches_dict, inplace=True)
            for l in range(len(weight_branches)):
                data = data.drop(columns=[weight_branches[l]])


            # if required, create and fill planing histograms
            if all_hists:
                if all_hists['do1D']:
                    all_hists_1D = all_hists['1D']
                    planing_1D_vars = all_hists['1D_vars']
                    use_eventweight_1D = all_hists['1D_use_eventweight']
                    for h,hist in enumerate(planing_1D_vars):
                        if use_eventweight_1D[h]:
                            all_hists_1D[h].fill(data[hist].values, weight=data['eventWeight'].values)
                        else:
                            all_hists_1D[h].fill(data[hist].values)
                    all_hists['1D'] = all_hists_1D
                if all_hists['do2D']:
                    all_hists_2D = all_hists['2D']
                    planing_2D_vars = all_hists['2D_names']
                    use_eventweight_2D = all_hists['2D_use_eventweight']
                    for h,hist in enumerate(planing_2D_vars):
                        if use_eventweight_2D[h]:
                            all_hists_2D[h].fill(data[hist[0]].values, data[hist[1]].values, weight=data['eventWeight'].values)
                        else:
                            all_hists_2D[h].fill(data[hist[0]].values, data[hist[1]].values)
                    all_hists['2D'] = all_hists_2D


            if save_datasets:
                # save the datasets
                name = infolder #filename.split('/')[-1].split('.')[0] # infolder+'_'+str(i)+'_'+tree
                if verbose:
                    print('\tDone reading!\n\tDataframe shape: ',data.shape)
                    #print('\tDataframe: \n',data)
                    cprint(f'\n\tStoring output dataframe to {outfolder}/{name}.h5', 'green')
                
                # Dump to hdf5 file
                desired_partition_size = partition_size
                if i==0:
                    time0 = datetime.now()
                    data = data.reset_index(drop=True)
                    #print(data.index)
                    total_memory_usage = data.memory_usage(deep=True).sum()
                    print('\ttotal size: ', total_memory_usage/(1024*1024), 'MB')
                    Npart = max(1, int(total_memory_usage/(desired_partition_size*1024*1024)))
                    if (Npart > 1): 
                        print('\tusing dask...')
                        data = dd.from_pandas(data, npartitions=Npart)
                    data.to_hdf(f'{outfolder}/{name}.h5', key=tree, mode='w', index=False)
                    time1 = datetime.now()
                    cprint(f'\tsaved directly in {time1-time0} \n','green')
                else:
                    time2 = datetime.now()
                    data_ = pd.read_hdf(f'{outfolder}/{name}.h5')
                    time3 = datetime.now()
                    cprint(f'\tread pandas in {time3 - time2}; \n\tstarting pandas concatenation for source file {start}... ','green')
                    data_ = pd.concat([data_, data], ignore_index=True)
                    time4 = datetime.now()
                    cprint(f'\tconcatenating concatenated in {time4-time3} ','green')
                    total_memory_usage = data_.memory_usage(deep=True).sum()
                    print('\ttotal size: ', total_memory_usage/(1024*1024), 'MB')
                    Npart = max(1, int(total_memory_usage/(desired_partition_size*1024*1024)))
                    if (Npart > 1):
                        print('\tusing dask...')
                        data_ = dd.from_pandas(data_, npartitions=Npart)
                    data_.to_hdf(f'{outfolder}/{name}.h5', key=tree, mode='r+', index=False)
                    time5 = datetime.now()
                    cprint(f'\tsaved after concatenation in {time5-time4} \n','green')

        except Exception as e:
            # Print failed files in red
            cprint('Failed file '+str(i), 'red')
            cprint(e, 'red')
            failed.append(i)

        i += 1

    if verbose:
        print('\tfinished with this file!\n')

    time_01 = datetime.now()
    cprint(f'\tdid everything for the current file in {time_01-time_00} \n','green')
    
    return i, failed, all_hists

def apply_scale(df, field_name, mode, pre_derived_scale=None):
    if pre_derived_scale:
        old_scale = pre_derived_scale
    else:
        old_scale = None
    if mode=='lognormalise':
        x, minimum, epsilon = apply_log(df[field_name], old_scale)
        x, mean, std = normalize(x, old_scale)
        scale = ("SaveLog / Normalize", minimum, epsilon, mean, std)
    elif mode=='normalise':
        x = df[field_name]
        x, mean, std = normalize(x, old_scale)
        scale = ("Normalize", mean, std)
    elif mode=='special':
        x = df[field_name]
        x = np.abs(x)**(1./3.) * np.sign(x)
        x, mean, std = normalize(x, old_scale)
        scale = ("Sqrt3", mean, std)
    else:
        raise ValueError('Scaling mode need for ', field_name)
    df[field_name] = x
    return df, scale

def normalize(x, pre_derived_scale=None):
    if pre_derived_scale:
        mean, std = pre_derived_scale[-2], pre_derived_scale[-1]
    else:
        mean, std = np.mean(x), np.std(x)
    out =  (x - mean) / std
    return out, mean, std

def apply_log(x, pre_derived_scale=None):
    if pre_derived_scale:
        minimum = pre_derived_scale[-4]
        epsilon = pre_derived_scale[-3]
    else:
        epsilon = 1e-10
        minimum = x.min()
    if minimum <= 0:
        x = x - minimum + epsilon
    else:
        minimum = 0
        epsilon = 0
    return np.log(x), minimum, epsilon