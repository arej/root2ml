import pickle as pkl
from utils.performance import *

samples = ['dijets', 'WprimePolL', 'WprimePolT']
variables1D = ['JpT', 'Jmass']
variables2D = ['J_pTvsMass']
filenames1D = [s+'_'+v+'_1D' for s in samples for v in variables1D]
filenames2D = [s+'_'+v+'_2D' for s in samples for v in variables2D]

print(filenames1D)
print(filenames2D)


for filename in filenames1D:
	with open('data/output/planing/'+filename+'.pkl', 'rb') as f:
		histo = pkl.load(f)
	plot_boost_histogram(histo, '1D', 'data/output/planing/'+filename+'.png', title=['large-R jet pT [MeV]'])
	print('done ', filename,'\n')

for filename in filenames2D:
        with open('data/output/planing/'+filename+'.pkl', 'rb') as f:
                histo = pkl.load(f)
        plot_boost_histogram(histo, '2D', 'data/output/planing/'+filename+'.png', title=['large-R jet pT [MeV]', 'large-R jet mass [MeV]'])
        print('done ', filename,'\n')

