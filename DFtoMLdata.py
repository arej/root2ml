import argparse
from utils.prepare import load_yaml, simplify_dict, apply_scale, class_dict
from utils.performance import normalize_bh
import pandas as pd
import numpy as np
import pickle as pkl
import boost_histogram as bh
from sklearn.model_selection import train_test_split

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert root inputs to h5 files')
    parser.add_argument('--config', required=True, help='configuration file')
    parser.add_argument('--verbose', '-v', action='store_true', help='Increase output verbosity')
    args = parser.parse_args()
    print('\tStarting...\n')

    config = load_yaml(args.config)
    outfolder = config['paths']['output_path']
    metadata_folder = config['paths']['metadata_path']
    planing_path = config['histogramming']['output_path']

    # getting names of samples and attaching sample-wide custom truth labels and features
    # like sig/bkg: 1/0, polarization: 0/PolL/PolT etc.
    samples_list = [
        config['ML_sample_preprocessing']['backgrounds'],
        config['ML_sample_preprocessing']['signals'], 
    ]
    samples_dataframes = []
    truth_features = config['ML_sample_preprocessing']['truth_features']
    truth_label = ['truth_label']

    # evaluate sum for planing weight calculation
    planing_integral_sum = {}
    planing_histo_per_class = {}
    planing_histo_min_bincontent_in_all_class = 1e30
    planing_integral_sum_total = 0
    extra_features_for_training = []
    if config['ML_sample_preprocessing']['use_planing']:
        if args.verbose:
            print('Getting integrals of planing histograms...')
        planing_variable_name = config['ML_sample_preprocessing']['planing_variable']
        extra_features_for_training = ['planing_weight']
        for slist,sample_list in enumerate(samples_list):
            planing_integral_sum[slist] = 0
            planing_histo_per_class[slist] = None #bh.Histogram(bh.axis.Regular(10, 0.0, 1.0)) #dummy definition
            
            for s, sample in enumerate(sample_list):
                with open(f'{planing_path}{sample[0]}_{planing_variable_name[0]}_{planing_variable_name[1]}.pkl', 'rb') as f:
                    histo_ = pkl.load(f)
                if s==0:
                    planing_histo_per_class[slist] = histo_.copy()
                else:
                    planing_histo_per_class[slist] += histo_
                planing_integral_sum[slist] += histo_.sum(flow=True)
                histo_contents = histo_.view()
                histo_contents_pos = histo_contents[histo_contents > 0]
                histo_min = histo_contents_pos.min()
                if histo_min < planing_histo_min_bincontent_in_all_class:
                    planing_histo_min_bincontent_in_all_class = histo_min

            planing_integral_sum_total += planing_integral_sum[slist]
        #max_planing_weight = planing_integral_sum_total/planing_histo_min_bincontent_in_all_class
        max_planing_weight = np.true_divide(planing_integral_sum_total, planing_histo_min_bincontent_in_all_class)
        max_planing_weight = np.nan_to_num(max_planing_weight, nan=0.0, posinf=0.0, neginf=0.0)

        if args.verbose:
            print('\tDone!\n')



    # reading dataframes of samples
    if args.verbose:
        print('reading dataframes of samples...')
    for slist,sample_list in enumerate(samples_list):
        for sample in sample_list:
            if args.verbose:
                print(f'\treading sample {outfolder}{sample[0]}.h5')
            sample_dataframe = pd.read_hdf(f'{outfolder}{sample[0]}.h5')
            print('\t of shape', sample_dataframe.shape)

            # unflatten nonSM samples in event weight
            if config['ML_sample_preprocessing']['unflatten_nonSM_samples_to_SM']:
                print("\t\t unflattening event weight to SM sample")
                # get here the SM (dijet) JpT_w histogram -> normalize
                with open(f"{planing_path}{config['ML_sample_preprocessing']['unflatten_target_SM_sample']}_{config['ML_sample_preprocessing']['unflatten_variable'][0]}_1D.pkl", 'rb') as f:
                    histo_unflatten_SM = pkl.load(f)
                histo_unflatten_SM = normalize_bh(histo_unflatten_SM)
                if sample[0] in config['ML_sample_preprocessing']['unflatten_nonSM_samples']:
                    # get here the nonSM sample[0]'s JpT_w histogram -> normalize
                    with open(f"{planing_path}{sample[0]}_{config['ML_sample_preprocessing']['unflatten_variable'][0]}_1D.pkl", 'rb') as f:
                        histo_unflatten_nonSM = pkl.load(f)
                    histo_unflatten_nonSM = normalize_bh(histo_unflatten_nonSM)
                    # divide by SM (dijet) normalized
                    histo_unflatten_ratio = histo_unflatten_SM/histo_unflatten_nonSM # !!!!! may need fixing divide by 0
                    # multiply factor to event weight of sample[0]
                    sample_dataframe['eventWeight'] *= histo_unflatten_ratio.values()[histo_unflatten_ratio.axes[0].index(sample_dataframe[config['ML_sample_preprocessing']['unflatten_variable'][1]])]


            # calculate planing weight
            if config['ML_sample_preprocessing']['use_planing']:
                if args.verbose:
                    print('\tevaluating and storing planing weights...')
                # with open(f'{planing_path}{sample[0]}_{planing_variable[0]}_{planing_variable[1]}.pkl', 'rb') as f:
                #     histo = pkl.load(f)
                histo = planing_histo_per_class[slist]

                if planing_variable_name[1] == '1D':
                    xname = planing_variable_name[0]
                    bin_contents = histo.values()[histo.axes[0].index(sample_dataframe[xname])]
                elif planing_variable_name[1] == '2D':
                    xname = config['histogramming']['variables_2D'][planing_variable_name[0]][0]
                    yname = config['histogramming']['variables_2D'][planing_variable_name[0]][1]
                    print(histo, histo.axes[0], histo.axes[1])
                    bin_contents = histo.values()[histo.axes[0].index(sample_dataframe[xname]), histo.axes[1].index(sample_dataframe[yname])]
                print(sample[0], "bin_contents: ", bin_contents, " length: ", len(bin_contents))
                
                # if config['ML_sample_preprocessing']['scale_all_to']:
                #     slist_scale_all_to = class_dict[config['ML_sample_preprocessing']['scale_all_to']]
                # else:
                #     slist_scale_all_to = slist
                # planing_weight = planing_integral_sum[slist_scale_all_to]/bin_contents
                # if slist==1:
                #     planing_weight *= config['ML_sample_preprocessing']['extra_scale_for_signal']

                # planing_weight = planing_integral_sum_total/bin_contents
                # planing_weight = planing_weight/max_planing_weight
                planing_weight = np.true_divide(planing_integral_sum_total, bin_contents)
                planing_weight = np.nan_to_num(planing_weight, nan=0.0, posinf=0.0, neginf=0.0)
                planing_weight = np.true_divide(planing_weight, max_planing_weight)
                planing_weight = np.nan_to_num(planing_weight, nan=0.0, posinf=0.0, neginf=0.0)

                print(sample[0], planing_weight, len(planing_weight))

                sample_dataframe['planing_weight'] = planing_weight

            # add truth features
            sample_dataframe[truth_label[0]] = slist # sig/bkg: 1/0
            if len(truth_features)>0:
                for s_feature in range(len(truth_features)): # e.g. polarization: 0/PolL/PolT
                    truth_feature_name = config['ML_sample_preprocessing']['truth_features'][s_feature] #'truth_feature_'+str(s_feature+1)
                    sample_dataframe[truth_feature_name] = sample[s_feature+1]
            samples_dataframes.append(sample_dataframe)



    # merge all samples 
    samples_dataframes = pd.concat(samples_dataframes, ignore_index=True)
    if args.verbose:
        print('total sample shape: ', samples_dataframes.shape,'\n')

    # save only a fraction if the sample is big
    if config['ML_sample_preprocessing']['save_partial']:
        samples_dataframes = samples_dataframes.sample(frac=config['ML_sample_preprocessing']['save_partial_frac'], random_state=1)


    # scale the input features in the dataframe and save the scale
    if args.verbose:
        print(f'applying and saving scale for input features at {metadata_folder}scales.txt ...\n')
    scales = {}
    input_features = list(simplify_dict(config['feature_branches'])[0].values())
    input_features_scale_mode = list(simplify_dict(config['feature_branches'])[1].values())
    for f,feature in enumerate(input_features):
        samples_dataframes, scales[feature] = apply_scale(samples_dataframes, feature, input_features_scale_mode[f])
    with open(f"{metadata_folder}scales.txt", "w") as f:
        f.write(str(scales))

    # other branches
    other_branches_1_dict = simplify_dict(config['other_branches_1'])[0]
    other_branches_1 = list(other_branches_1_dict.values())
    other_branches_2_dict = simplify_dict(config['other_branches_2'])[0]
    other_branches_2 = list(other_branches_2_dict.values())
    event_weight_branch = ['eventWeight']
    dsid_branch = ['dsid']

    # divide sample in train(validation inclusive) and test datasets
    # and prepare them
    if args.verbose:
        print('splitting train, validation and test datasets ...\n')
    train_df, test_df = train_test_split(samples_dataframes, test_size=config['ML_sample_preprocessing']['split_train_val_test'][2])
    train_df, val_df = train_test_split(train_df, test_size=1.*config['ML_sample_preprocessing']['split_train_val_test'][1]/(config['ML_sample_preprocessing']['split_train_val_test'][0]+config['ML_sample_preprocessing']['split_train_val_test'][1]))

    train_df = pd.DataFrame(train_df, 
        columns=input_features+truth_label+truth_features+extra_features_for_training+event_weight_branch+other_branches_1)
    val_df = pd.DataFrame(val_df, 
        columns=input_features+truth_label+truth_features+extra_features_for_training+event_weight_branch+other_branches_1)
    test_df = pd.DataFrame(test_df, 
        columns=input_features+truth_label+truth_features+extra_features_for_training+event_weight_branch+other_branches_1+other_branches_2+dsid_branch)

    if args.verbose:
        print('converting train and test datasets to numpy ...\n')
    train_npy = train_df.values
    val_npy = val_df.values
    test_npy = test_df.values

    print(f'Saving training data at {outfolder}train.npy of shape {train_npy.shape}')
    np.save(f'{outfolder}train.npy', train_npy)
    print(f'Saving validation data at {outfolder}val.npy of shape {val_npy.shape}')
    np.save(f'{outfolder}val.npy', val_npy)
    print(f'Saving test data at {outfolder}test.npy of shape {test_npy.shape} \n')
    np.save(f'{outfolder}test.npy', test_npy)

    """
    print(f'Saving training data at {outfolder}train.h5 of shape {train_df.shape}')
    train_df.to_hdf(f'{outfolder}train.h5', key='train', mode='w')
    print(f'Saving test data at {outfolder}test.h5 of shape {test_df.shape}')
    test_df.to_hdf(f'{outfolder}test.h5', key='test', mode='w')
    """

    if args.verbose:
        print('Done! \n\t datasets ready!')
