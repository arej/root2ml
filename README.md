### Purpose: 
To create ML datasets with high level features from ROOT files, with a highly text-based configurable setup


### To convert all ROOT files of samples from many dsid and campaigns to single dataframes as sample.h5, use: 
```
python root2DF.py --config configs/polTagger.yaml --verbose
```

for which the options in the following parts have to be set in the configuration file:
1. `paths`
2. `samples_metadata`
3. `skimming`
4. `samples`
5. `eventweight_branches`, `feature_branches`, `other_branches`
 
also, histograms can be created for sample balancing/planing or plotting: 6. `histogramming`


### To arrange all sample dataframes and convert them to train and test datasets, use:
```
python DFtoMLdata.py --config configs/polTagger.yaml --verbose
```

for which `ML_sample_preprocessing` options have to be set in the configuration file.

###### The following are saved in the train dataset:
- ML input features
- signal/background lable
- planing/sample balancing related weights
- any additional truth lables or features

###### The following are also saved in the test dataset in addition to the above:
- event weight using `eventweight_branches` and `samples_metadata`
- sample dsid

###### Additional test macros are kept in _tests/_ folder

###### An overall diagram is shown with 
- inputs shown in _black_ background 
- coding framework in _green_ background
- outputs generated in _blue_ background
- possible future upgrades in _red_ arrows

![alt text](docs/diagram.png)
