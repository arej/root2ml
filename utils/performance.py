from scipy.stats import entropy
import matplotlib.pyplot as plt

def add_flow(bh_hist):
    values = bh_hist.values()
    values_with_flow = bh_hist.values(flow=True)
    if len(values.shape)==1:
        values[0] += values_with_flow[0] # add underflow
        values[-1] += values_with_flow[-1] # add overflow
    elif len(values.shape)==2:
        for r in range(values.shape[0]):
            values[r,0] += values_with_flow[r+1,0] # add underflow column
            values[r,-1] += values_with_flow[r+1,-1] # add overflow column
        for c in range(values.shape[1]):
            values[0,c] += values_with_flow[0,c+1] # add underflow row
            values[-1,c] += values_with_flow[-1,c+1] # add overflow row

    return bh_hist

def normalize_bh(bh_hist):
    bh_hist_new = bh_hist/sum(bh_hist.values(flow=True))
    return bh_hist_new

def plot_boost_histogram(bh_hist, dim, path, title=[], flow=True):
    if dim=='1D':
        #plt.bar(bh_hist.axes[0].centers, bh_hist.values(), width=bh_hist.axes[0].widths)
        #plt.step(bh_hist.axes[0].edges[:-1], bh_hist.values(), where="post")
        if flow:    add_flow(bh_hist)
        plt.stairs(bh_hist.values(), bh_hist.axes[0].edges)
        if title:
            plt.xlabel(title[0])
    elif dim=='2D':
        if flow:    add_flow(bh_hist)
        fig_kw = dict(figsize = (8,6))
        fig, ax = plt.subplots(**fig_kw)
        mesh = ax.pcolormesh(*bh_hist.axes.edges.T, bh_hist.view().T)
        if title:
            ax.set(xlabel = title[0])
            ax.set(ylabel = title[1])
        fig.colorbar(mesh)

    plt.savefig(path)
    plt.close()
    return 0
