import numpy as np
import matplotlib.pyplot as plt

inpath = '../root2ml/data/output/v0.2_2409/' #'/eos/home-a/arej/samples_VjetsAllHad/MLdata/Nov24/'
outpath = 'data/output/v0.2_2409/planing/planing_weight_1D.png'

data = np.load(inpath+'train.npy', allow_pickle=True)
weight = data[:, 13].astype('float64') # planing weight
label = data[:, 11].astype('intc') # truth lable
weight_sig = weight[label==1]
weight_bkg = weight[label==0]

#bins = np.linspace(min(weight_sig.min(), weight_bkg.min()), max(weight_sig.max(), weight_bkg.max()), 10)
bins = np.linspace(0., 1., 1000)

counts_sig, bins_, _ = plt.hist(weight_sig, histtype='step', bins=bins, label='signal')
counts_bkg, bins_, _ = plt.hist(weight_bkg, histtype='step', bins=bins, label='background')

#bin_widths = np.diff(bins_)
bin_indices_sig = np.digitize(weight_sig, bins_) - 1
average_weights_sig = [weight_sig[bin_indices_sig == i].mean() if np.any(bin_indices_sig == i) else 0 for i in range(len(bins_) - 1)]
bin_indices_bkg = np.digitize(weight_bkg, bins_) - 1
average_weights_bkg = [weight_bkg[bin_indices_bkg == i].mean() if np.any(bin_indices_bkg == i) else 0 for i in range(len(bins_) - 1)]
weight_total_sig = np.sum(counts_sig * average_weights_sig)
weight_total_bkg = np.sum(counts_bkg * average_weights_bkg)

print("total counts: signal - ", np.sum(counts_sig), ", background - ", np.sum(counts_bkg))
print("weight integrals: signal - ", np.sum(counts_sig * average_weights_sig), 
					", background - ", np.sum(counts_bkg * average_weights_bkg))
print("weighted total signal/background: ", weight_total_sig/weight_total_bkg)

plt.yscale('log')
plt.legend()
plt.savefig(outpath)
print('\tsaved plot in', outpath, '\n')
plt.show()
plt.close()