import numpy as np
import matplotlib.pyplot as plt

dataset = np.load('data/output/test.npy', allow_pickle=True)

LJet_pT_bkg = dataset[dataset[:,20]==0, 11]
planing_weight_bkg = dataset[dataset[:,20]==0, 19]

LJet_pT_sig = dataset[dataset[:,20]==1, 11]
planing_weight_sig = dataset[dataset[:,20]==1, 19]

print('background shapes: ', LJet_pT_bkg.shape, planing_weight_bkg.shape)
print('signal shapes: ', LJet_pT_sig.shape, planing_weight_sig.shape)

fig_kw = dict(figsize = (12,5))
fig, axs = plt.subplots(nrows=1, ncols=2, **fig_kw)

axs[0].scatter(planing_weight_bkg, LJet_pT_bkg, c='b', label='background')
axs[0].scatter(planing_weight_sig, LJet_pT_sig, c='r', label='signal')
axs.flat[0].set(xlabel = 'planing weight')
axs.flat[0].set(ylabel = 'large-R jet pT [MeV]')
axs[0].legend(loc='upper left')

planing_weight_bins = [0,5,10,15,20,25,30,35,40,45,50,55]
axs[1].hist(planing_weight_bkg, bins=planing_weight_bins, color='blue', label='background', histtype='step', fill=False)
axs[1].hist(planing_weight_sig, bins=planing_weight_bins, color='red', label='signal', histtype='step', fill=False)
axs.flat[1].set(xlabel = 'planing weight')
axs[1].legend(loc='upper left')

plt.savefig('data/output/planing/planing_weight_JpT.png')
plt.close()