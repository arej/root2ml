import argparse
from utils.prepare import *
from utils.performance import plot_boost_histogram
from termcolor import cprint
import io
import pandas as pd
import boost_histogram as bh
import pickle

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert root inputs to h5 files')
    parser.add_argument('--config', required=True, help='configuration file to convert')
    parser.add_argument('--verbose', '-v', action='store_true', help='Increase output verbosity')
    args = parser.parse_args()

    config = load_yaml(args.config)


    if args.verbose:
        print('\tReading sample metadata...\n')
    # reading sample metadata
    samples_cross_section_file = config['samples_metadata']['xsec_list']
    if samples_cross_section_file:
        with open(samples_cross_section_file, "r") as f:
            samples_cross_section_list = f.readlines()
        samples_cross_section_list = [config['samples_metadata']['xsec_columnNames']]+samples_cross_section_list
        df_xsec = pd.read_csv(io.StringIO(''.join(samples_cross_section_list)), sep='\s+')
        df_xsec['xsec'] = df_xsec['xsec1']*df_xsec['xsec2']

    xsec_unit_in_fb = config['samples_metadata']['xsec_unit_in_fb']

    samples_sum_weights_file = config['samples_metadata']['sum_weights_list']
    if samples_sum_weights_file:
        with open(samples_sum_weights_file, "r") as f:
            samples_sum_weights_list = f.readlines()
        samples_sum_weights_list = [config['samples_metadata']['sum_weights_columnNames']]+samples_sum_weights_list
        df_sumwt = pd.read_csv(io.StringIO(''.join(samples_sum_weights_list)), sep='\s+')

    lumi_dict = config['samples_metadata']['intlumi']


    if args.verbose:
        print('\tPreparing list of branches...\n')
    # preparing list of branches
    nominal_tree = config['paths']['nominal_tree_name']
    weight_branches = config['eventweight_branches']
    feature_branches_dict = simplify_dict(config['feature_branches'])[0]
    feature_branches = list(feature_branches_dict.keys())
    other_branches_1_dict = simplify_dict(config['other_branches_1'])[0]
    other_branches_1 = list(other_branches_1_dict.keys())
    other_branches_2_dict = simplify_dict(config['other_branches_2'])[0]
    other_branches_2 = list(other_branches_2_dict.keys())
    all_required_branches = weight_branches+feature_branches+other_branches_1+other_branches_2
    if args.verbose:
        print('weight Branches: ', weight_branches,'\n')
        print('feature branches: ', feature_branches,'\n')
        print('other Branches: ', other_branches_1,'\n', other_branches_2,'\n')
    rename_branches_dict = {} 
    rename_branches_dict.update(feature_branches_dict)
    rename_branches_dict.update(other_branches_1_dict)
    rename_branches_dict.update(other_branches_2_dict)
    prune_outlier_branches_dict = simplify_dict(config['feature_branches'])[2]



    # creating dictionary of all the root files as required in config
    input_root_path = config['paths']['input_path']
    samples = config['samples']
    input_files = {}
    input_sample_xsec = {}
    input_sample_sumw = {}
    input_sample_lumi = {}
    input_sample_dsid = {}
    for sample_list in samples:
        input_files[sample_list] = []
        input_sample_xsec[sample_list] = []
        input_sample_sumw[sample_list] = []
        input_sample_lumi[sample_list] = []
        input_sample_dsid[sample_list] = []
        sample_list_names = config['samples'][sample_list]
        for sample in sample_list_names:
            input_files[sample_list] += find_files(input_root_path, sample[0], args.verbose)
            input_sample_xsec[sample_list].append(df_xsec.loc[df_xsec['dsid']==sample[1], 'xsec'].values[0])
            input_sample_sumw[sample_list].append(df_sumwt.loc[(df_sumwt['dsid']==sample[1]) & (df_sumwt['camp']==sample[2]), 'sumWeights'].values[0])
            input_sample_lumi[sample_list].append(lumi_dict[sample[2]])
            input_sample_dsid[sample_list].append(sample[1])
    if args.verbose:
        print('input files: \n',input_files)
        print('input files cross-sections: \n',input_sample_xsec)
        print('input files sum-weights: \n',input_sample_sumw)
        print('input files lumi: \n',input_sample_lumi)
        print('input files dsid: \n',input_sample_dsid)



    # define histograms 
    Histogram_flag = config['histogramming']['create']
    all_hists = {}
    if Histogram_flag:
        all_hists['do1D'] = config['histogramming']['dimension_1D']
        all_hists['do2D'] = config['histogramming']['dimension_2D']
        if config['histogramming']['dimension_1D']:
            all_hists_1D = {}
            all_hists['1D_vars'] = []
            all_hists['1D_use_eventweight'] = []
            for v,var in enumerate(config['histogramming']['variables_1D']):
                if config['histogramming']['bin_regular_1D'][v]:
                    all_hists_1D[v] = bh.Histogram(
                        bh.axis.Regular(
                            config['histogramming']['regular_1D_binning'][v][0],
                            config['histogramming']['regular_1D_binning'][v][1],
                            config['histogramming']['regular_1D_binning'][v][2],
                        )
                    )
                else:
                    all_hists_1D[v] = bh.Histogram(
                        bh.axis.Variable(config['histogramming']['variable_1D_binning'][v]), 
                    )
                all_hists['1D_vars'].append(var)
                all_hists['1D_use_eventweight'].append(config['histogramming']['1D_use_eventweight'][v])
            all_hists['1D'] = all_hists_1D
        if config['histogramming']['dimension_2D']:
            all_hists_2D = {}
            all_hists['2D_names'] = []
            all_hists['2D_use_eventweight'] = []
            for v,var in enumerate(config['histogramming']['variables_2D']):
                if config['histogramming']['bin_regular_2D'][v]:
                    all_hists_2D[v] = bh.Histogram(
                        bh.axis.Regular(
                            config['histogramming']['regular_2D_binning'][v][0][0],
                            config['histogramming']['regular_2D_binning'][v][0][1],
                            config['histogramming']['regular_2D_binning'][v][0][2],
                        ),
                        bh.axis.Regular(
                            config['histogramming']['regular_2D_binning'][v][1][0],
                            config['histogramming']['regular_2D_binning'][v][1][1],
                            config['histogramming']['regular_2D_binning'][v][1][2],
                        ),
                    )
                else:
                    all_hists_2D[v] = bh.Histogram(
                        bh.axis.Variable(config['histogramming']['variable_2D_binning'][v][0]), 
                        bh.axis.Variable(config['histogramming']['variable_2D_binning'][v][1]),
                    )
                all_hists['2D_names'].append(config['histogramming']['variables_2D'][var])
                all_hists['2D_use_eventweight'].append(config['histogramming']['2D_use_eventweight'][v])
            all_hists['2D'] = all_hists_2D



    # converting the files from the input_files dictionary into h5 files
    outfolder = config['paths']['output_path']
    failed_all = []

    cuts = config['skimming']['event_selection'] # ['largeRjet_pt_NOSYS < 3000000.']
    cuts = ' & '.join(['('+x+')' for x in cuts])

    for infolder, files in input_files.items():
        cprint('Running on '+infolder+': '+str(files), 'magenta')
        i=0
        # kwargs for uproot
        kwargs = dict(expressions=all_required_branches, cut=cuts, step_size=config['skimming']['uproot_chunk_size'])
        for j, file in enumerate(files):
            metadata = [input_sample_lumi[infolder][j]*float(xsec_unit_in_fb)*(input_sample_xsec[infolder][j]/input_sample_sumw[infolder][j])] #metadata factor
            metadata.append(input_sample_dsid[infolder][j]) #dsid
            if args.verbose:
                print('processing file ', j, ': ', file)
                #print('\tcross-section*int.Lumi/sumWeights: ', metadata)

            start = i
            i, failed, all_hists = convertROOTtoHDF5(
                file, 
                start, 
                infolder, 
                outfolder, 
                metadata = metadata, 
                weight_branches=weight_branches, 
                rename_branches_dict=rename_branches_dict, 
                tree=nominal_tree, 
                all_hists = all_hists, 
                save_datasets = config['skimming']["save_datasets"],
                prune_outliers=config['skimming']['prune_outlier_of_selected_features'], 
                prune_outliers_dict=prune_outlier_branches_dict, 
                partition_size = config['skimming']['partition_size_for_dask'],
                verbose=args.verbose, **kwargs)
            failed_all += failed


        if Histogram_flag:
            for v,var in enumerate(config['histogramming']['variables_1D_names']):
                with open(f"{config['histogramming']['output_path']}{infolder}_{var}_1D.pkl", "wb") as f:
                    pickle.dump(all_hists['1D'][v], f)
                    print("\tCreated Histogram: ", f"{config['histogramming']['output_path']}{infolder}_{var}_1D.pkl")
                f.close()
                if config['histogramming']['plot']:
                    plot_boost_histogram(all_hists['1D'][v], '1D', f"{config['histogramming']['output_path']}{infolder}_{var}_1D.png", flow=config['histogramming']['addFlowBins_ToLastBins'])
                    print("\tSaved Histogram: ", f"{config['histogramming']['output_path']}{infolder}_{var}_1D.png")    
                all_hists['1D'][v].reset()
            for v,var in enumerate(config['histogramming']['variables_2D']):
                with open(f"{config['histogramming']['output_path']}/{infolder}_{var}_2D.pkl", "wb") as f:
                    pickle.dump(all_hists['2D'][v], f)
                    print("\tCreated Histogram: ", f"{config['histogramming']['output_path']}{infolder}_{var}_2D.pkl")
                f.close()
                if config['histogramming']['plot']:
                    plot_boost_histogram(all_hists['2D'][v], '2D', f"{config['histogramming']['output_path']}{infolder}_{var}_2D.png", flow=config['histogramming']['addFlowBins_ToLastBins'])
                    print("\tSaved Histogram: ", f"{config['histogramming']['output_path']}{infolder}_{var}_2D.png")
                all_hists['2D'][v].reset()

        print('\n\tEnd of the process', infolder)



    if failed_all:
    	cprint(failed_all, 'red')

